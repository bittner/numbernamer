#!/usr/bin/env python3
"""
Large number namer
Inspired by the German super brain.

Source for German names of numbers:
- http://mathe-abakus.fraedrich.de/mathematik/grzahlen.html
"""
try:
    base = int(input("Basis: [42] ") or 42)
    exp = int(input("Exponent: [99] ") or 99)
except ValueError:
    raise SystemExit("Bitte eine (positive) ganze Zahl eingeben.")

number = str(base ** exp)

print('{base}^{exp} = {number} wird gesprochen:'.format(
    base=base,
    exp=exp,
    number=number
))

indices = list(range(len(number), 0, -3))
indices.reverse()

with open('numbernames_de.txt') as namesfile:
    names = namesfile.read().splitlines()[0:len(indices)]

names.reverse()
before = 0

for count, next in enumerate(indices):
    print('{number: >3} {name}'.format(
        number=number[before:next],
        name=names[count],
    ))
    before = next
